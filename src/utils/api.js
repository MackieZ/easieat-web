export const DOMAIN_NAME = 'http://localhost:8080/'
// export const DOMAIN_NAME = 'http://104.248.146.169:8080'

export const API_PREFIX = 'api/v1/'

export const API_ADDRESS = DOMAIN_NAME + API_PREFIX

const API = {
  FOOD_LIST: 'food/list',
  FOOD_SET: 'food/addfood',
  FOOD_EDIT: 'food/edit',
  FOOD_DELETE: 'food/remove',
  TALLE_LIST: 'table/list',
  TALLE_SET: 'table/setTable',
  TALLE_DELETE: 'table/deleteFood',
}
export const getApiEditFood = () => {
  return API_ADDRESS + API.FOOD_EDIT
}

export const getApiFoodList = (restaurantId = 1) => {

  return API_ADDRESS + API.FOOD_LIST
}

export const getApiSetFood = () => {
  return API_ADDRESS + API.FOOD_SET
}

export const getApiDeleteFood = () => {
  return API_ADDRESS + API.FOOD_DELETE
}

export const getApiTableList = () => {
  return API_ADDRESS + API.TABLE_LIST + queryString
}

export const getApiSetTable = () => {
  return API_ADDRESS + API.TABLE_SET
}

export const getApiDeleteTable = () => {
  return API_ADDRESS + API.TABLE_DELETE
}

/**
 * convert data to FormData for API
 * if you're using this function with transformRequest option in axios
 * bare in mind that it only applicable for request methods 'PUT', 'POST', and 'PATCH'
 *
 * @param   {object} data - data object
 * @return  {FormData} - convertedData
 */
export const convertToFormData = data => {
  const formData = new FormData()
  for (const [key, value] of Object.entries(data)) {
    if (value !== null && value !== undefined) {
      formData.append(key, value)
    }
  }

  return formData
}
