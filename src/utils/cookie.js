/**
 * create cookies.
 * @param   {string} name  name of cookie
 * @param   {object} value value store in cookie
 * @param   {number} sec   time cookie expire in second.
 * @param   {string} [path='/']   specify cookie path
 */
export function create(name, value, sec, path = '/') {
  let expires = ''

  if (sec) {
    const date = new Date()
    date.setTime(date.getTime() + sec * 1000)
    expires = `; expires=${date.toUTCString()}`
  }

  document.cookie = `${name}=${value + expires}; path=${path};`
}

/**
 * read cookie from cookie's name
 * @param   {string} name name of target cookie to read.
 * @return  {string|null}  cookie value in string or null if cookie not found.
 */
export function read(name) {
  const nameEQ = `${name}=`
  const ca = document.cookie.split(';')
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i]
    while (c.charAt(0) === ' ') c = c.substring(1, c.length)
    if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length)
  }
  return null
}

/**
 * remove target cookie
 * @param  {string} name cookie name to remove
 */
export function remove(name) {
  create(name, '', -1)
}

const cookie = {
  create,
  read,
  remove,
}

export default cookie
