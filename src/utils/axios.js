import axios from 'axios'
import { read } from '../utils/cookie'

export const axiosWrapper = axios

axiosWrapper.interceptors.request.use((config) => {
  const token = read('token')
  if (token != null) {
    config.headers = { 'x-access-token': token }
  }
  return config
})

export default axiosWrapper