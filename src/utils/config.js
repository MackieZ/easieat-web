export const FOOD_TYPE = [
  {
    id: 1,
    category: 'อาหารคาว',
  },
  {
    id: 2,
    category: 'อาหารเจและมังสวิรัติ',
  },
  {
    id: 3,
    category: 'เครื่องดื่ม',
  },
  {
    id: 4,
    category: 'อาหารว่าง',
  },
  {
    id: 5,
    category: 'อาหารหวาน',
  },
  {
    id: 6,
    category: 'อื่นๆ',
  },
]
