import PageExample from './pages/PageExample'
import { Route } from 'react-router-dom'
import React from 'react'
import PageAuth from './pages/PageAuth'
import PageFood from './pages/PageFood'
import PageQueue from './pages/PageQueue'
import PageTable from './pages/PageTable'
import PageOrder from './pages/PageOrder'
import PageOrderChief from './pages/PageOrderChief'
import PagePayment from './pages/PagePayment'

export const pathRoutes = {
  Example: {
    path: '/example',
    component: PageExample,
  },
  Register: {
    path: '/auth',
    component: PageAuth,
  },
  Queue: {
    path: '/queue',
    component: PageQueue,
  },
  Food: {
    path: '/food',
    component: PageFood,
  },
  Table: {
    path: '/table',
    component: PageTable,
  },
  Order: {
    path: '/order',
    component: PageOrder,
  },
  OrderChief: {
    path: '/orderChief',
    component: PageOrderChief,
  },
  Payment: {
    path: '/payment',
    component: PagePayment
  }
}

export const Routes = Object.values(pathRoutes).map(route => (
  <Route key={`route-${route.path}`} {...route} exact />
))

Routes.push(<Route key={'Landing-1'} path="/" exact component={PageExample} />)
Routes.push(<Route key="NotFound" render={() => <div>404 Page not found.</div>} />)

export default Routes
