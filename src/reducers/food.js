import axios from 'axios'
import { getApiFoodList, getApiSetFood, getApiDeleteFood, getApiEditFood, convertToFormData } from '../utils/api'

export const initialState = {
  foodList: [],
  isLoading: false,
  error: null,
}

// TYPES
export const types = {
  FETCH_FOOD_LIST: 'FOOD/FETCH_FOOD_LIST',
  FETCH_FOOD_LIST_SUCCESS: 'FOOD/FETCH_FOOD_LIST_SUCCESS',
  FETCH_FOOD_LIST_FAILED: 'FOOD/FETCH_FOOD_LIST_FAILED',
  SET_FOOD: 'FOOD/SET_FOOD',
  SET_FOOD_SUCCESS: 'FOOD/SET_FOOD_SUCCESS',
  SET_FOOD_FAILED: 'FOOD/SET_FOOD_FAILED',
  ADD_FOOD: 'FOOD/ADD_FOOD',
  DELETE_FOOD: 'FOOD/DELETE_FOOD',
  EDIT_FOOD: 'FOOD/EDIT_FOOD'
}

export default function food(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_FOOD_LIST: {
      return { ...state, isLoading: true, error: null }
    }

    case types.FETCH_FOOD_LIST_SUCCESS: {
      return { ...state, foodList: action.payload.foodList, error: null, isLoading: false }
    }

    case types.FETCH_FOOD_LIST_FAILED: {
      return { ...state, error: action.payload.error, isLoading: false }
    }

    case types.ADD_FOOD: {
      const foodId = action.payload.food.foodId
      const foodList = state.foodList.filter(food => food.foodId !== foodId)

      return { ...state, foodList: [...foodList, action.payload.food] }
    }

    case types.DELETE_FOOD: {
      const foodId = action.payload.food.foodId
      const foodList = state.foodList.filter(food => food.foodId !== foodId)

      return { ...state, foodList }
    }

    case types.EDIT_FOOD: {
      const foodId = action.payload.food.foodId;
      const foodList = state.foodList.filter(food => food.foodId !== foodId)
      return { ...state, foodList: [...foodList, action.payload.food] }
    }

    default: {
      return state
    }
  }
}

const fetchListAPI = ({ restaurantId = 1 }) => {
  return axios.get(getApiFoodList(restaurantId))
}

const fetchFoodList = () => async dispatch => {
  // set loading
  dispatch({ type: types.FETCH_FOOD_LIST })

  try {
    const foodList = await fetchListAPI({})
    console.log("FOODLIST",foodList)
    dispatch({ type: types.FETCH_FOOD_LIST_SUCCESS, payload: { foodList: foodList.data } })

    return { foodList }
  } catch (error) {
    console.error(error)
    dispatch({ type: types.FETCH_FOOD_LIST_FAILED, payload: { error } })

    return error
  }
}

const setFoodApi = ({
  restaurantId = 1,
  name = 'สตอเบอร์รี่ชีสเค้ก',
  price = 30,
  foodImage = null,
  type = 'ขนมเค้ก',
  foodId = undefined,
  url = null,
}) => {
  return axios({
    method: 'post',
    url: getApiSetFood(),
    data: {
      foodName: name,
      foodPrices: price,
      foodType: type,
      file: foodImage,
    },
    transformRequest: [convertToFormData],
  })
}

const deleteFoodApi = ({ foodId = 0 }) => {
  return axios({
    method: 'get',
    url: getApiDeleteFood() + '/' + foodId,
  })
}

const setFood = ({
  name,
  price,
  foodImage,
  type,
  foodId,
  url,
}) => async dispatch => {
  try {
    const setFood = await setFoodApi({
      name,
      price,
      foodImage: foodImage && foodImage.originFileObj,
      type,
      foodId,
      url,
    })

    console.log(setFood)
    if(setFood.data.error) {
      return null
    }

    dispatch({ type: types.ADD_FOOD, payload: { food: setFood.data.data }})

    return { setFood }
  } catch (error) {
    console.error(error)
    //  dispatch({ type: types.FETCH_FOOD_LIST_FAILED, payload: { error } })

    return error
  }
}

const deleteFood = ({ foodId = 0 }) => async dispatch => {
  try {
    const deleteFood = await deleteFoodApi({ foodId })
    let foodID_ = foodId;
    dispatch({ type: types.DELETE_FOOD, payload: { food: { foodId : foodID_ }} })
  } catch (error) {
    console.error(error)

    return error
  }
}
const editFood = ({ foodId, name, type, price, foodImage }) => async dispatch => {
  console.log(foodId, name, type, price , foodImage)
  const callApiEditFood = await axios({
    method: 'POST',
    url: getApiEditFood(),
    data: {
      foodId: foodId,
      foodName: name,
      foodPrices: price,
      foodType: type,
      file: foodImage && foodImage.originFileObj,
    },
    transformRequest: [convertToFormData],
  })
  dispatch({ 
    type: types.EDIT_FOOD,
    payload: { 
      food: callApiEditFood.data.data
    }
  })

}

export const actions = {
  fetchFoodList,
  setFood,
  editFood,
  deleteFood,
}
