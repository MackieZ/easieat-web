import styled from 'styled-components'
import COLORS from '../styles/colors'

export const styles = {
  paddingY: 8,
  paddingX: 12,
  borderRadius: 5,
}

export const Button = styled.a`
  padding: ${styles.paddingY}px ${styles.paddingX}px;
  background-color: ${props => props.bg || COLORS.RED};
  background-image: ${props =>
    props.bg ||
    `linear-gradient(to right, ${COLORS.GRADIENT_START} 0%, ${COLORS.GRADIENT_STOP} 100%)`};
  color: ${props => props.color || COLORS.WHITE};
  text-align: center;
  border-radius: ${styles.borderRadius}px;
  border: 0 none;
  outline: none;
  cursor: pointer;
  position: relative;

  &:hover,
  &:active {
    color: ${props => props.color || COLORS.WHITE};
    opacity: 0.9;
  }

  &[disabled] {
    &:before {
      content: '';
      background: white;
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      width: 100%;
      height: 100%;
      opacity: 0.2;
    }
  }
`

export const OutlineButton = styled(Button)`
  background-color: transparent;
  background-image: none;
  color: ${props => props.color || COLORS.RED};
  border: 1px solid ${props => props.borderColor || COLORS.RED};

  &:hover,
  &:active {
    color: ${props => props.color || COLORS.RED};
  }
`

export default Button
