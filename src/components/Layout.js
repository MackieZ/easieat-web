import styled from 'styled-components'
import COLORS from '../styles/colors'

const Layout = styled.div`
  min-height: 100vh;
  background: ${COLORS.GRAY};
  padding: 2rem;
  position: relative;
  display: flex;
  flex-direction: column;
  padding-bottom: 6em;
`

export default Layout
