import React from 'react'
import styled from 'styled-components'
import COLORS from '../styles/colors'
import Button from './Button'
import { withRouter } from 'react-router-dom'
import { FoodList, Order, Payment, QRCode, Queue, Setting } from '../styles/icons'
import { remove } from '../utils/cookie'
import { message } from 'antd'

export const styles = {
  menu: {
    active: {
      height: 30,
      width: 30,
      fill: `${COLORS.WHITE}`,
    },
    normal: {
      height: 30,
      width: 30,
      fill: `${COLORS.RED}`,
    },
  },
}

const Wrapper = styled.div`
  position: fixed;
  bottom: 0;
  border-top: 2px solid ${COLORS.GRAY};
  width: 100%;
  height: 75px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${COLORS.WHITE};
  z-index: 99;
`

const MenuButton = styled(Button)`
  box-sizing: border-box;
  border-radius: 0;
  height: 100%;
  width: 128px;

  ${props =>
  !props.active &&
  `
    background: ${COLORS.WHITE};
    color: ${COLORS.RED};
    
    &:hover,
    &:active {
      color: ${COLORS.RED};
    }
  `}
`

const MenuIcon = styled.div`
  margin: 0 auto;
`

const MenuText = styled.div``

export const NavbarBottom = ({ activeKey = '1', history }) => {
  const onRouteChange = route => {
    history.push(route)
  }

  const onLogout = () => {
    remove('token')
    message.success('Logout success')

    history.push('/auth')
  }

  return (
    <Wrapper>
      <MenuButton active={activeKey === '1'} onClick={() => onRouteChange('/food')}>
        <MenuIcon>
          <FoodList {...styles.menu[activeKey === '1' ? 'active' : 'normal']} />
        </MenuIcon>
        <MenuText>รายการอาหาร</MenuText>
      </MenuButton>

      {/* <MenuButton active={activeKey === '2'}>
      <MenuIcon>
        <Promotion {...styles.menu[activeKey === '2' ? 'active' : 'normal']} />
      </MenuIcon>
      <MenuText>โปรโมชั่น</MenuText>
    </MenuButton> */}
      <MenuButton active={activeKey === '3'} onClick={() => onRouteChange('/table')}>
        <MenuIcon>
          <QRCode {...styles.menu[activeKey === '3' ? 'active' : 'normal']} />
        </MenuIcon>
        <MenuText>จัดการโต๊ะ</MenuText>
      </MenuButton>
      {/* <MenuButton active={activeKey === '4'}>
      <MenuIcon>
        <Waiter {...styles.menu[activeKey === '4' ? 'active' : 'normal']} />
      </MenuIcon>
      <MenuText>พนักงาน</MenuText>
    </MenuButton> */}
      {/* <MenuButton active={activeKey === '5'}>
      <MenuIcon>
        <Report {...styles.menu[activeKey === '5' ? 'active' : 'normal']} />
      </MenuIcon>
      <MenuText>รายงาน</MenuText>
    </MenuButton> */}
      <MenuButton active={activeKey === '6'} onClick={() => onRouteChange('/queue')}>
        <MenuIcon>
          <Queue {...styles.menu[activeKey === '6' ? 'active' : 'normal']} />
        </MenuIcon>
        <MenuText>จัดการคิว</MenuText>
      </MenuButton>
      <MenuButton active={activeKey === '7'} onClick={() => onRouteChange('/order')}>
        <MenuIcon>
          <Order {...styles.menu[activeKey === '7' ? 'active' : 'normal']} />
        </MenuIcon>
        <MenuText>ออเดอร์</MenuText>
      </MenuButton>
      {/*  <MenuButton active={activeKey === '8'}>*/}
      {/*  <MenuIcon>*/}
      {/*    <Setting {...styles.menu[activeKey === '8' ? 'active' : 'normal']} />*/}
      {/*  </MenuIcon>*/}
      {/*  <MenuText>ตั้งค่า</MenuText>*/}
      {/*</MenuButton>*/}
      <MenuButton active={activeKey === '8'} onClick={() => onRouteChange('payment')}>
        <MenuIcon>
          <Payment {...styles.menu[activeKey === '8' ? 'active' : 'normal']} />
        </MenuIcon>
        <MenuText>ชำระเงิน</MenuText>
      </MenuButton>
      <MenuButton active={activeKey === '9'} onClick={() => onLogout()}>
        <MenuIcon>
          <Setting {...styles.menu[activeKey === '9' ? 'active' : 'normal']} />
        </MenuIcon>
        <MenuText>ออกจากระบบ</MenuText>
      </MenuButton>
    </Wrapper>
  )
}

export default withRouter(NavbarBottom)
