import React from 'react'
import styled from 'styled-components'
import Button, { OutlineButton } from './Button'
import COLORS from '../styles/colors'
import LogoGroup from '../assets/logo_group.svg'
import { Spin } from 'antd'
import { pathRoutes } from '../routes'
import { Link } from 'react-router-dom'

const SpinWrapper = styled.div`
  margin-top: 16px;
`

const SpinOverride = styled(Spin)`
  .ant-spin-dot i {
    background-color: ${COLORS.RED};
  }
`

const Layout = styled.div`
  background-color: #3f3f3f;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin: auto;
  font-size: 16px;
  color: ${COLORS.WHITE};
`

const ComponentWrapper = styled.div`
  margin-top: 20px;
  display: grid;
  grid-template: 1fr / 1fr 1fr;
  grid-column-gap: 25px;
`

const ImgLogo = styled.img`
  width: 220px;
  height: 70px;
`

const AntdWrapper = styled.div`
  text-align: center;
  margin-top: 20px;
`

const RoutingWrapper = styled.div`
  text-align: center;
`

const RoutingHead = styled.div``

const LinkPage = styled(Link)`
  text-decoration: underline;
  margin: 0 10px;
`

const Example = () => {
  return (
    <Layout>
      <ImgLogo src={LogoGroup} /> <br />
      Example Components
      <ComponentWrapper>
        <OutlineButton>Outline Button</OutlineButton>
        <Button>Normal Button</Button>
      </ComponentWrapper>
      <AntdWrapper>
        Antd Override
        <SpinWrapper>
          <SpinOverride size={'large'} />
        </SpinWrapper>
      </AntdWrapper>
      <RoutingWrapper>
        <RoutingHead>Pages</RoutingHead>
        {Object.values(pathRoutes).map(({ path }) => (
          <LinkPage key={`route-${path}`} to={path}>
            {path.split('/')[1]}
          </LinkPage>
        ))}
      </RoutingWrapper>
    </Layout>
  )
}

export default Example
