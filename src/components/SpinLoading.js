import { Spin } from 'antd'
import { COLORS } from '../styles/colors'
import styled from 'styled-components'
import React from 'react'

const SpinOverride = styled(Spin)`
  .ant-spin-dot i {
    background-color: ${COLORS.RED};
  }
`

const SpinWrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const SpinLoading = () => (
  <SpinWrapper>
    <SpinOverride />
  </SpinWrapper>
)

export default SpinLoading
