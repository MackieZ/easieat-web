import React from 'react'
import styled from 'styled-components'
import COLORS from '../styles/colors'

const Card = styled.div`
  padding: ${props => props.padding || '1.5rem'};
  min-height: ${props => props.minHeight || 'calc(100vh - 12rem)'};
  background: ${props => props.color || COLORS.WHITE};
`

export const CardOverride = props => <Card {...props} />

export default CardOverride
