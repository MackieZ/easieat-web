import React from 'react'
import { compose, withProps } from 'recompose'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps'

const MapComponent = compose(
  withProps({
    googleMapURL:
      'https://maps.googleapis.com/maps/api/js?key=AIzaSyCHLjQGtkkvQUq_XO0z6QhBzSMhXh_l2Ho',
    loadingElement: <div style={{ height: '100%' }} />,
    containerElement: <div style={{ height: '300px' }} />,
    mapElement: <div style={{ height: '100%' }} />,
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap
    defaultZoom={13}
    defaultCenter={props.defaultCenter || { lat: -34.397, lng: 150.644 }}
    onBoundsChanged={props.onBoundsChanged}
    // center={props.defaultCenter}
  >
    {props.isMarkerShown && (
      <Marker
        position={props.defaultCenter || { lat: -34.397, lng: 150.644 }}
        onClick={props.onMarkerClick}
      />
    )}
  </GoogleMap>
))

class GoogleMapEnhance extends React.PureComponent {
  state = {
    isMarkerShown: false,
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      position => {
        const { latitude, longitude } = position.coords
        const { setLocation } = this.props

        setLocation({ latitude, longtitude: longitude })

        this.setState({
          userLocation: { lat: latitude, lng: longitude },
          loading: false,
        })
      },
      () => {
        this.setState({ loading: false })
      }
    )

    this.delayedShowMarker()
  }

  delayedShowMarker = () => {
    setTimeout(() => {
      this.setState({ isMarkerShown: true })
    }, 3000)
  }

  handleMarkerClick = () => {
    this.setState({ isMarkerShown: false })
    this.delayedShowMarker()
  }

  onBoundsChanged = () =>
    navigator.geolocation.getCurrentPosition(
      position => {
        const { latitude, longtitude } = position.coords

        this.setState({
          center: { lat: latitude, lng: longtitude },
          loading: false,
        })
      },
      () => {
        this.setState({ loading: false })
      }
    )

  render() {
    return (
      <MapComponent
        isMarkerShown={this.state.isMarkerShown}
        onMarkerClick={this.handleMarkerClick}
        defaultCenter={this.state.userLocation}
        onBoundsChanged={this.onBoundsChanged}
      />
    )
  }
}

export default GoogleMapEnhance
