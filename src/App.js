import React, { Component } from 'react'
import { hot } from 'react-hot-loader'
import styled, { createGlobalStyle } from 'styled-components'
import COLORS from './styles/colors'
import Routes from './routes'
import { Switch } from 'react-router-dom'
import 'antd/dist/antd.less'

const GlobalStyle = createGlobalStyle`
  html, body {
    height: 100%;
  }

  body {
    font-family: 'Kanit', sans-serif !important;
    font-weight: normal !important;
    font-size: 16px !important;
    line-height: 1.5;

    margin: 0;
    padding: 0;
  }
`

const MainLayout = styled.div`
  background-color: ${COLORS.WHITE};
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin: auto;
  color: ${COLORS.BLACK};
`

const Content = styled.div`
  height: auto;
  width: 100%;
`

class App extends Component {
  render() {
    return (
      <MainLayout>
        <GlobalStyle/>
        <Content>
          <Switch>{Routes}</Switch>
        </Content>
      </MainLayout>
    )
  }
}

export default hot(module)(App)
