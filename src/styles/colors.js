export const COLORS = {
  RED: '#FF4747',
  ORANGE: '#FF8329',
  WHITE: '#FFFFFF',
  BLACK: '#000000',
  GRAY: '#efefef',

  GRADIENT_START: '#FF4747',
  GRADIENT_STOP: '#FF8329',
}

export default COLORS
