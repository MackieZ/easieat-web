import React from 'react'
import Layout from '../components/Layout'
import NavbarBottom from '../components/NavbarBottom'
import styled from 'styled-components'
import SpinLoading from '../components/SpinLoading'
import { TableTagWrapper, TableTag } from './PageTable'
import axiosWrapper from '../utils/axios'
import { API_ADDRESS } from '../utils/api'
import { message, Tabs as AntdTabs } from 'antd'
import { FOOD_TYPE } from '../utils/config'
import CardOverride from '../components/CardOverride'
import COLORS from '../styles/colors'
import { actions } from '../reducers/food'
import { connect } from 'react-redux'
import Button from '../components/Button'

const Wrapper = styled.div``

const GridLayoutMenu = styled.div`
  display: grid;
  grid-template-columns: auto 350px;
  grid-gap: 1em;
`

const Title = styled.h1`
  display: inline-flex;
  align-items: center;
  font-size: 1.5em;
  font-weight: 600;
  margin-bottom: 0;
`

const Header = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
`

const TabPane = AntdTabs.TabPane

const GridLayout = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-column-gap: 25px;
  grid-row-gap: 25px;

  @media (max-width: 768px) {
    grid-template-columns: 1fr 1fr;
  }
`

const CardWrapper = styled.div`
  display: block;
  background: ${COLORS.GRAY};
  width: 100%;
  position: relative;
  cursor: pointer;

  &:hover > .card-edit {
    display: flex;
  }
`

const FoodImgWrapper = styled.div`
  height: 200px;
`

const FoodImg = styled.img`
  //RATIO FROM 300 x 250 = 0.83;
  width: 100%;
  height: 100%;
`

const DetailsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: absolute;
  background: ${COLORS.WHITE};
  bottom: 0;
  width: 100%;
  opacity: 0.8;

  padding: 8px 12px;
`

const DetailsName = styled.div`
  color: ${COLORS.BLACK};
`

const DetailsPrice = styled.div`
  color: ${COLORS.RED};
  font-weight: bold;
`

const Tabs = styled(AntdTabs)`
  &.ant-tabs.ant-tabs-card .ant-tabs-card-bar .ant-tabs-tab-active {
    color: ${COLORS.RED};
  }

  .ant-tabs-nav .ant-tabs-tab:hover {
    color: ${COLORS.RED};
  }
`

const MenuWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const MenuHeader = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 1em;
  background: ${COLORS.WHITE};
`

const MenuTitle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0.5em;
  font-weight: 600;
`

const OrderListWrapper = styled.div`
  background: ${COLORS.WHITE};
  flex: 1;
  padding: 0.5em;
`

const OrderButtonWrapper = styled.div`
  margin-top: 1em;
`

const OrderButton = styled(Button)`
  display: block;
`

const OrderItem = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 1em;
`

const AmountWrapper = styled.div`
  min-width: 100px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const AmountButton = styled.div`
  padding: 0.5em;
  border: 1px solid ${COLORS.RED};
  border-radius: 5px;
  width: 30px;
  height: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 1em;
  cursor: pointer;
  color: ${COLORS.RED};
`

class PageOrder extends React.Component {
  state = {
    isLoading: true,
    error: null,
    selectedTable: null,
    selectedFoodIndexes: [],
    tableList: [],
    orderList: [],
  }

  componentDidMount() {
    const { fetchFoodList } = this.props

    const getTableData = async () => {
      try {
        const { data } = await axiosWrapper.get(`${API_ADDRESS}table/list`)

        const tableList = data.map(table => ({
          ...table,
          title: table.tableName,
          code: table.SerialNumber,
          isEditing: false,
          fieldValue: '',
        }))

        this.setState({ isLoading: false, tableList })
      } catch (error) {
        message.error(error)

        // eslint-disable-next-line react/no-did-mount-set-state
        this.setState({ isLoading: false })
      }
    }
    getTableData()
    fetchFoodList()
  }

  onAddOrder = (food, index) => {
    const newOrder = {
      ...food,
      amount: 1,
    }

    let { selectedFoodIndexes } = this.state
    if (selectedFoodIndexes.indexOf(index) > -1) {
      return null
    }

    selectedFoodIndexes.push(index)

    this.setState({ orderList: [...this.state.orderList, newOrder], selectedFoodIndexes })
  }

  onSelctedTable = (table, index) => {
    this.setState({ selectedTable: table })
  }

  handleDecreaseOrder = index => {
    const { orderList } = this.state
    if (orderList[index].amount > 0) {
      orderList[index].amount -= 1
    }

    this.setState({ orderList })
  }

  handleIncreaseOrder = index => {
    const { orderList } = this.state
    orderList[index].amount += 1

    this.setState({ orderList })
  }

  onConfirmOrder = async () => {
    const { data } = await axiosWrapper.post(`${API_ADDRESS}order/addbyowner`, {
      tableID: this.state.selectedTable._id,
      foodList: this.state.orderList.map(order => ({ ...order, quantity: order.amount })),
    })

    if (!data.error) {
      message.success(data.message)
    } else {
      return message.error(data.message)
    }

    this.setState({
      isLoading: false,
      error: null,
      selectedTable: null,
      selectedFoodIndexes: [],
      orderList: [],
    })
  }

  render() {
    const { isLoading = true, error = null, tableList = [], orderList = [] } = this.state
    const { foodList = [] } = this.props

    const orders = orderList.map((order, index) => (
      <OrderItem key={index}>
        {order.name}
        <AmountWrapper>
          <AmountButton onClick={() => this.handleDecreaseOrder(index)}>-</AmountButton>
          {order.amount}
          <AmountButton onClick={() => this.handleIncreaseOrder(index)}>+</AmountButton>
        </AmountWrapper>
      </OrderItem>
    ))

    const tables = tableList.map((table, index) => (
      <TableTag
        active={false}
        key={`tableTags-${index}`}
        onClick={() => this.onSelctedTable(table, index)}
      >
        {table.title}
      </TableTag>
    ))

    const enhanceFoodType = [
      {
        id: 0,
        category: 'ทั้งหมด',
      },
      ...FOOD_TYPE,
    ]

    const tabsFood = enhanceFoodType.map(({ category, id }) => {
      const foodItems = foodList
        .filter(({ type }) => type === category || category === 'ทั้งหมด')
        .map((food, index) => {
          return (
            <CardWrapper key={`food-${food.foodId}`} onClick={() => this.onAddOrder(food, index)}>
              <FoodImgWrapper>
                <FoodImg src={food.url} />
              </FoodImgWrapper>
              <DetailsWrapper>
                <DetailsName> {food.name} </DetailsName>
                <DetailsPrice> ฿{food.price} </DetailsPrice>
              </DetailsWrapper>
            </CardWrapper>
          )
        })

      return (
        <TabPane tab={category} key={id}>
          <CardOverride>
            <GridLayout>{foodItems}</GridLayout>
          </CardOverride>
        </TabPane>
      )
    })

    const selectTableState = (
      <Header>
        <Title>เลือกโต๊ะ</Title>
        <TableTagWrapper>{tables}</TableTagWrapper>
      </Header>
    )

    const selectFoodState = (
      <GridLayoutMenu>
        <Tabs type="card" title="food" defaultActiveKey="0" key="1">
          {tabsFood}
        </Tabs>
        <MenuWrapper>
          <MenuHeader>
            โต๊ะ {(this.state.selectedTable && this.state.selectedTable.tableName) || ''}
          </MenuHeader>
          <MenuTitle>
            <Wrapper>รายการ</Wrapper>
            <Wrapper>จำนวน</Wrapper>
          </MenuTitle>
          <OrderListWrapper>{orders}</OrderListWrapper>
          <OrderButtonWrapper>
            <OrderButton onClick={this.onConfirmOrder}>สั่งอาหาร</OrderButton>
          </OrderButtonWrapper>
        </MenuWrapper>
      </GridLayoutMenu>
    )

    if (isLoading) {
      return (
        <Wrapper>
          <Layout>
            <SpinLoading />
          </Layout>
          <NavbarBottom activeKey="7" />
        </Wrapper>
      )
    }

    if (!isLoading && error) {
      return (
        <Wrapper>
          <Layout>ไม่สามารถเชื่อมต่อกับฐานข้อมูลได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง</Layout>
          <NavbarBottom activeKey="7" />
        </Wrapper>
      )
    }

    return (
      <Wrapper>
        <Layout>{!this.state.selectedTable ? selectTableState : selectFoodState}</Layout>

        <NavbarBottom activeKey="7" />
      </Wrapper>
    )
  }
}

const mapStateToProps = state => ({
  foodList: state.food.foodList,
})

const mapDispatchToProps = {
  fetchFoodList: actions.fetchFoodList,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PageOrder)
