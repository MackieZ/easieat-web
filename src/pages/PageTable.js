import React from 'react'
import Layout from '../components/Layout'
import NavbarBottom from '../components/NavbarBottom'
import styled from 'styled-components'
import Button from '../components/Button'
import { COLORS } from '../styles/colors'
import QRCode from 'qrcode-react'
import { Input, Modal, Icon, message } from 'antd'
import SpinLoading from '../components/SpinLoading'
import EDIT_ICON from '../assets/edit.svg'
import DELETE_ICON from '../assets/rubbish-bin.svg'
import axiosWrapper from '../utils/axios'
import { API_ADDRESS } from '../utils/api'

const Wrapper = styled.div``

const Title = styled.h1`
  font-size: 1.5em;
  font-weight: 600;
`

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const AddButton = styled(Button)`
  min-width: 150px;
`

export const TableTagWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  margin-top: 1em;
`

export const TableTag = styled.div`
  padding: 0.5em 1.5em;
  border-radius: 5px;
  border: 1px solid ${COLORS.RED};
  color: ${COLORS.RED};
  margin-right: 1em;
  margin-bottom: 1em;
  cursor: pointer;

  &:hover {
    background: ${COLORS.RED};
    color: ${COLORS.WHITE};
  }

  ${props =>
    props.active &&
    `
    background: ${COLORS.RED};
    color: ${COLORS.WHITE};
  `}
`

const TableCard = styled.div`
  background: ${COLORS.WHITE};
  padding: 1em 5em;
  margin: 0 auto;

  text-align: center;
`

const Code = styled.div`
  font-size: 1.25em;
  font-weight: 600;
  color: ${COLORS.RED};
  margin-bottom: 1em;
`

const DownloadWrapper = styled.div`
  margin-top: 2em;
  display: flex;

  a {
    width: 100%;
  }
`

const ButtonFooterWrapper = styled.div`
  width: 100%;
  display: flex;
  margin: 0.5rem;
  margin-left: auto;
  justify-content: space-between;
  align-items: center;
  padding-left: 8px;
  padding-right: 8px;

  > * {
    flex: 1;
  }
`

const TableItem = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0.5em;
  border-bottom: 1px solid ${COLORS.GRAY};
`

const Action = styled.div`
  width: 60px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const ActionItem = styled.img`
  width: 20px;
  height: 20px;
  cursor: pointer;
`

const AddField = styled.div`
  display: grid;
  grid-template-columns: auto 40px;
  grid-column-gap: 10px;
  color: ${COLORS.RED};
  align-items: center;
  margin: 5px 0;
  font-size: 1.55em;

  i {
    cursor: pointer;
  }
`

class PageTable extends React.Component {
  state = {
    showingModal: false,
    isLoading: true,
    error: null,
    tableList: [
      {
        title: 'โต๊ะเบอร์ 1',
        code: 'ABC123',
        isEditing: false,
        fieldValue: '',
      },
      {
        title: 'โต๊ะเบอร์ 2',
        code: 'ABC153',
        isEditing: false,
        fieldValue: '',
      },
      {
        title: 'โต๊ะเบอร์ 3',
        code: 'ABC143',
        isEditing: false,
        fieldValue: '',
      },
    ],
    selectedIndex: 0,
    isAdding: false,
    fieldValue: '',
  }

  componentDidMount() {
    const getTableData = async () => {
      try {
        const { data } = await axiosWrapper.get(`${API_ADDRESS}table/list`)

        const tableList = data.map(table => ({
          ...table,
          title: table.tableName,
          code: table.SerialNumber,
          isEditing: false,
          fieldValue: '',
        }))

        this.setState({ isLoading: false, tableList })
      } catch (error) {
        message.error(error)

        // eslint-disable-next-line react/no-did-mount-set-state
        this.setState({ isLoading: false })
      }
    }
    getTableData()
  }

  download(code = 'T001') {
    const link = document.createElement('a')
    link.download = `${code}.png`
    link.href = document.querySelector('canvas').toDataURL()
    link.click()
  }

  onAddTable = async () => {
    const { fieldValue } = this.state

    try {
      const { data } = await axiosWrapper.post(`${API_ADDRESS}table/add`, {
        tableName: fieldValue,
      })

      if (!data.error) {
        message.success(data.message)
      }

      const newTable = {
        title: data.data.tableName,
        code: data.data.SerialNumber,
        isEditing: false,
        fieldValue: '',
      }

      const { tableList } = this.state

      this.setState({ tableList: [...tableList, newTable], isAdding: false, fieldValue: '' })
    } catch (message) {
      message.error(message)
    }
  }

  onDeleteItem = async (table, index) => {
    try {
      const { data } = await axiosWrapper.get(`${API_ADDRESS}table/remove/${table._id}`)

      if (!data.error) {
        message.success(data.message)
      }

      const { tableList } = this.state

      tableList.splice(index, 1)
      this.setState({ tableList, selectedIndex: 0 })

    } catch (message) {
      message.error(message)
    }
  }

  onEditItem = (index) => {
    const { tableList } = this.state

    tableList[index].isEditing = true

    this.setState({ tableList })
  }

  onTagClick = index => {
    this.setState({ selectedIndex: index })
  }

  onShowModal = () => {
    const { tableList } = this.state

    const newTableList = tableList.map(table => ({ ...table, isEditing: false }))

    this.setState({ showingModal: true, tableList: newTableList })
  }

  handleAddFieldValue = e => {
    this.setState({ fieldValue: e.target.value })
  }

  handleEditFieldValue = (e, index) => {
    const { tableList } = this.state

    tableList[index].fieldValue = e.target.value

    this.setState({ tableList })
  }

  onEditTableName = async (table, index) => {

    try {
      const { data } = await axiosWrapper.post(`${API_ADDRESS}table/edit/`, {
        tableName: table.fieldValue,
        tableID: table._id,
      })

      if (!data.error) {
        message.success(data.message)
      }

      const { tableList } = this.state

      tableList[index].title = tableList[index].fieldValue
      tableList[index].isEditing = false

      this.setState({ tableList })

    } catch (message) {
      message.error(message)
    }

  }

  render() {
    const {
      isLoading = true,
      error = null,
      tableList = [],
      showingModal,
      selectedIndex,
    } = this.state

    if (isLoading) {
      return (
        <Wrapper>
          <Layout>
            <SpinLoading />
          </Layout>
          <NavbarBottom activeKey="3" />
        </Wrapper>
      )
    }

    if (!isLoading && error) {
      return (
        <Wrapper>
          <Layout>ไม่สามารถเชื่อมต่อกับฐานข้อมูลได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง</Layout>
          <NavbarBottom activeKey="3" />
        </Wrapper>
      )
    }

    const tags = tableList.map((table, index) => (
      <TableTag
        active={selectedIndex === index}
        key={`tableTags-${index}`}
        onClick={() => this.onTagClick(index)}
      >
        {table.title}
      </TableTag>
    ))

    const tablesItems = tableList.map((table, index) => {
      if (!table.isEditing) {
        return (
          <TableItem key={`tableItems-${index}`}>
            <Wrapper>{table.title}</Wrapper>
            <Action>
              <Wrapper>
                <ActionItem src={EDIT_ICON} onClick={() => this.onEditItem(index)} />
              </Wrapper>
              <Wrapper onClick={() => this.onDeleteItem(table, index)}>
                <ActionItem src={DELETE_ICON} />
              </Wrapper>
            </Action>
          </TableItem>
        )
      }

      return (
        <AddField key={index}>
          <Input
            placeholder={table.title}
            value={table.fieldValue}
            onChange={e => this.handleEditFieldValue(e, index)}
          />
          <Icon type={'check-circle'} onClick={() => this.onEditTableName(table, index)} />
        </AddField>
      )
    })

    return (
      <Wrapper>
        <Layout>
          <Header>
            <Title>จัดการโต๊ะ</Title>
            <AddButton onClick={this.onShowModal}>จัดการโต๊ะ</AddButton>
          </Header>
          <TableTagWrapper>{tags}</TableTagWrapper>
          {tableList.length > 0 ? (
            <TableCard>
              <Title>{tableList[selectedIndex].title}</Title>
              <Code>{tableList[selectedIndex].code}</Code>
              <QRCode value={tableList[selectedIndex].code} size={256} />
              <DownloadWrapper>
                <Button onClick={() => this.download(tableList[selectedIndex].code)}>
                  ดาวน์โหลดคิวอาร์โค้ด
                </Button>
              </DownloadWrapper>
            </TableCard>
          ) : (
            ''
          )}
        </Layout>

        <Modal
          title="เพิ่มโต๊ะ"
          visible={showingModal}
          onCancel={() => this.setState({ showingModal: false })}
          footer={[
            <ButtonFooterWrapper key={'BTN_FOOTER'}>
              {!this.state.isAdding ? (
                <Button
                  key="submit"
                  type="primary"
                  onClick={() => this.setState({ isAdding: true })}
                >
                  เพิ่มโต๊ะ
                </Button>
              ) : (
                <AddField>
                  <Input
                    placeholder={'โต๊ะเบอร์ 1'}
                    onChange={this.handleAddFieldValue}
                    value={this.state.fieldValue}
                  />
                  <Icon type={'check-circle'} onClick={() => this.onAddTable()} />
                </AddField>
              )}
            </ButtonFooterWrapper>,
          ]}
        >
          {tablesItems}
        </Modal>
        <NavbarBottom activeKey="3" />
      </Wrapper>
    )
  }
}

export default PageTable
