import React from 'react'
import Layout from '../components/Layout'
import NavbarBottom from '../components/NavbarBottom'
import styled from 'styled-components'
import Button from '../components/Button'
import { COLORS } from '../styles/colors'
import { Modal, Input, message } from 'antd'
import SpinLoading from '../components/SpinLoading'
import { API_ADDRESS } from '../utils/api'
import axiosWrapper from '../utils/axios'

const Wrapper = styled.div``

const Title = styled.h1`
  display: inline-flex;
  align-items: center;
  font-size: 1.5em;
  font-weight: 600;
  margin-bottom: 0;
`

const AddQueueButton = styled(Button)`
  position: absolute;
  top: 0;
  right: 0;
  width: 200px;
`

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 1.5em;
  position: relative;
`

const RedDot = styled.div`
  display: inline-flex;
  color: ${COLORS.WHITE};
  justify-content: center;
  align-items: center;
  font-size: 16px;
  width: 20px;
  height: 20px;
  background: ${COLORS.RED};
  border-radius: 50%;

  margin: ${props => props.margin};
`

const QueueBox = styled.div`
  display: inline-flex;
  align-items: center;
  flex-direction: column;

  padding: 1em 2em;
  background: ${COLORS.WHITE};
  box-shadow: 1px 1px 3px -1px;
  cursor: pointer;
  border-radius: 10px;

  &:not(:last-child) {
    margin-right: 3em;
    margin-bottom: 3em;
  }
`

const ButtonFooterWrapper = styled.div`
  width: 100%;
  display: flex;
  margin: 0.5rem;
  margin-left: auto;
  justify-content: space-between;
  align-items: center;

  > * {
    flex: 1;
  }
`

const QueueBoxWrapper = styled.div`
  margin: 1em 0;
`

const QueueNumber = styled.div`
  color: ${COLORS.RED};
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 10px 0;
  font-size: 2em;
`

const QueueName = styled.div`
  text-align: center;
  font-weight: 600;
`

const QueuePeople = styled.div`
  color: ${COLORS.ORANGE};
`

const AddQueueGridLayout = styled.div`
  display: grid;
  grid-template-rows: 1fr 1fr;
  grid-template-columns: 1fr 2fr;
  grid-column-gap: 1em;
  grid-row-gap: 1em;
`

const PeopleButtonGridLayout = styled.div`
  display: grid;
  grid-template-rows: 1fr 1fr;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-column-gap: 0.5em;
  grid-row-gap: 0.5em;
`

const PeopleButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1.25em;
  color: #4f4f4f;
  border: 1px solid #4f4f4f;
  border-radius: 10px;
  cursor: pointer;

  &:hover {
    border: none;
    color: ${COLORS.WHITE};
    background: ${COLORS.RED};
  }

  ${props =>
    props.selected
      ? `
    color: ${COLORS.WHITE};
    border: none;
    background: ${COLORS.RED};
  `
      : ''}
`

class PageQueue extends React.Component {
  state = {
    restaurantID: null,
    isLoading: true,
    showingModal: false,
    error: null,
    recentQueues: [],
    queueLists: [],
    peopleName: '',
    peopleAmount: 1,
  }

  handleQueue = async (queue, queueIndex, queueStage = 'waiting') => {
    const { queueLists, recentQueues } = this.state
    if (queueStage === 'waiting') {
      const { data } = await axiosWrapper.post(`${API_ADDRESS}queue/activequeue`, {
        queueID: queue._id,
      })

      if (data.error) {
        message.error(data.message)
      }

      const newRecentQueues = [queueLists.splice(queueIndex, 1)[0], ...recentQueues]

      this.setState({ recentQueues: newRecentQueues, queueLists })
    }

    if (queueStage === 'recent') {
      recentQueues.splice(queueIndex, 1)

      this.setState({ recentQueues })
    }
  }

  onAddQueue = async () => {
    const { data } = await axiosWrapper.post(`${API_ADDRESS}queue/newrestaurantqueue`, {
      totalPeople: this.state.peopleAmount,
      Name: this.state.peopleName,
    })

    if (data.error) {
      message.error(data.message)
    } else {
      message.success(data.message)
    }

    this.setState({
      showingModal: false,
      peopleName: '',
      peopleAmount: 1,
    })
  }

  onPayment = () => {
    console.log('ON PAYMENT')

    this.setState({ showingPayment: false })
  }

  componentDidMount() {
    let interval
    const getRestaurantQueue = async () => {
      clearInterval(this.state.interval)
      try {
        const { data } = await axiosWrapper.get(`${API_ADDRESS}queue/getrestaurantqueue`)
        console.log(data)
        const queueLists = data.map(queue => ({
          ...queue,
        }))

        const sortQueueList = queueLists
          .sort((a, b) => {
            const diff = +new Date(b.createdAt) - +new Date(a.createdAt)
            return diff
          })
          .filter(queue => queue.inQueue)

        const recentQueueList = queueLists.filter(queue => queue.inRestaurant)

        // eslint-disable-next-line react/no-did-mount-set-state
        this.setState({
          isLoading: false,
          queueLists: sortQueueList,
          recentQueues: recentQueueList,
        })

        // return data
      } catch (error) {
        message.error(error)

        // eslint-disable-next-line react/no-did-mount-set-state
        this.setState({ isLoading: false })

        // return null
      }

      interval = setTimeout(getRestaurantQueue, 5000)
      this.setState({ interval })
    }
    getRestaurantQueue()
  }

  componentWillUnmount() {
    clearInterval(this.state.interval)
  }

  render() {
    const {
      isLoading = true,
      error = null,
      recentQueues,
      queueLists,
      showingModal = false,
      showingPayment = false,
    } = this.state

    const queueListsItem = queueLists.map((queue, index) => (
      <QueueBox key={`recent-${index}`} onClick={() => this.handleQueue(queue, index)}>
        <QueueNumber>{queue.queueNumber}</QueueNumber>
        <QueueName>{queue.queueName}</QueueName>
        <QueuePeople>จำนวนคน: {queue.totalPeople}</QueuePeople>
      </QueueBox>
    ))

    const recentQueueList = recentQueues.map((queue, index) => (
      <QueueBox key={`recent-${index}`} onClick={() => this.handleQueue(queue, index, 'recent')}>
        <QueueNumber>{queue.queueNumber}</QueueNumber>
        <QueueName>{queue.queueName}</QueueName>
        <QueuePeople>จำนวนคน: {queue.totalPeople}</QueuePeople>
      </QueueBox>
    ))

    const peopleButtons = [1, 2, 3, 4, 5, 6, 7, 8].map(number => {
      return (
        <PeopleButton
          onClick={() => this.setState({ peopleAmount: number })}
          selected={this.state.peopleAmount === number}
          key={number}
        >
          {number}
        </PeopleButton>
      )
    })

    if (isLoading) {
      return (
        <Wrapper>
          <Layout>
            <SpinLoading />
          </Layout>
          <NavbarBottom activeKey="6" />
        </Wrapper>
      )
    }

    if (!isLoading && error) {
      return (
        <Wrapper>
          <Layout>ไม่สามารถเชื่อมต่อกับฐานข้อมูลได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง</Layout>
          <NavbarBottom activeKey="6" />
        </Wrapper>
      )
    }

    return (
      <Wrapper>
        <Layout>
          <Header>
            <AddQueueButton onClick={() => this.setState({ showingModal: true })}>
              + เพิ่มคิว
            </AddQueueButton>
            <Title>
              คิวล่าสุด <RedDot margin={'0 10px'}>{recentQueues.length}</RedDot>
            </Title>
          </Header>
          <QueueBoxWrapper>{recentQueueList}</QueueBoxWrapper>
          <Header>
            <Title>คิวที่รออยู่ </Title>
            <Title>
              จำนวนคิว <RedDot margin={'0 10px'}>{queueLists.length}</RedDot>
            </Title>
          </Header>
          <QueueBoxWrapper>{queueListsItem}</QueueBoxWrapper>
        </Layout>
        <Modal
          title="เพิ่มคิว"
          visible={showingModal}
          onCancel={() => this.setState({ showingModal: false })}
          footer={[
            <ButtonFooterWrapper key={'BTN_FOOTER'}>
              <React.Fragment>
                <Button key="submit" type="primary" onClick={() => this.onAddQueue()}>
                  เพิ่มคิว
                </Button>
              </React.Fragment>
            </ButtonFooterWrapper>,
          ]}
        >
          <AddQueueGridLayout>
            <Wrapper>ชื่อลูกค้า</Wrapper>
            <Input
              onChange={e => this.setState({ peopleName: e.target.value })}
              value={this.state.peopleName}
            />
            <Wrapper>จำนวนลูกค้า</Wrapper>
            <PeopleButtonGridLayout>{peopleButtons}</PeopleButtonGridLayout>
          </AddQueueGridLayout>
        </Modal>
        <NavbarBottom activeKey="6" />
      </Wrapper>
    )
  }
}

export default PageQueue
