import React from 'react'
import NavbarBottom from '../components/NavbarBottom'
import Layout from '../components/Layout'
import styled from 'styled-components'
import { Tabs as AntdTabs, Modal, Input, Upload, Icon, Select, message } from 'antd'
import CardOverride from '../components/CardOverride'
import Button, { OutlineButton } from '../components/Button'
import SpinLoading from '../components/SpinLoading'
import COLORS from '../styles/colors'
import { actions } from '../reducers/food'
import { connect } from 'react-redux'
import { DOMAIN_NAME } from '../utils/api'
import { FOOD_TYPE } from '../utils/config'
import { read } from '../utils/cookie'

const Option = Select.Option

const TabPane = AntdTabs.TabPane

const Wrapper = styled.div``

const AddFoodButton = styled(Button)`
  position: absolute;
  top: 0;
  right: 0;
`

const GridLayout = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-column-gap: 25px;
  grid-row-gap: 25px;

  @media (max-width: 768px) {
    grid-template-columns: 1fr 1fr;
  }
`

const CardWrapper = styled.div`
  display: block;
  background: ${COLORS.GRAY};
  width: 100%;
  position: relative;
  cursor: pointer;

  &:hover > .card-edit {
    display: flex;
  }
`

const FoodImgWrapper = styled.div`
  height: 200px;
`

const FoodImg = styled.img`
  //RATIO FROM 300 x 250 = 0.83;
  width: 100%;
  height: 100%;
`

const DetailsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: absolute;
  background: ${COLORS.WHITE};
  bottom: 0;
  width: 100%;
  opacity: 0.8;

  padding: 8px 12px;
`

const DetailsName = styled.div`
  color: ${COLORS.BLACK};
`

const DetailsPrice = styled.div`
  color: ${COLORS.RED};
  font-weight: bold;
`

const GridContentWrapper = styled.div`
  display: grid;
  grid-template: 1fr 1fr 1fr / 2fr 1fr 2fr;
  grid-row-gap: 1rem;
  grid-column-gap: 1.5rem;
`

const TextLabel = styled.div``

const UploadImgBox = styled(Upload)`
  display: flex;
  justify-content: center;
  align-items: center;
  grid-row: 1 / span 3;
  position: relative;

  cursor: pointer;

  > .ant-upload.ant-upload-select-picture-card {
    width: 100%;
    height: 100%;
    margin: 0;
  }
`

const TextInput = styled(Input)`
  &.ant-input {
    width: 100% !important;
  }
`

const Tabs = styled(AntdTabs)`
  &.ant-tabs.ant-tabs-card .ant-tabs-card-bar .ant-tabs-tab-active {
    color: ${COLORS.RED};
  }

  .ant-tabs-nav .ant-tabs-tab:hover {
    color: ${COLORS.RED};
  }
`

const ButtonFooterWrapper = styled.div`
  width: 100%;
  display: flex;
  margin: 0.5rem;
  margin-left: auto;
  justify-content: space-between;
  align-items: center;

  a {
    ${props =>
      props.isEdit
        ? `
      width: 50%;

      &:last-child {
        margin-left: 24px;
      }
    `
        : 'width: 100%;'}; 
`

const CardEdit = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  position: absolute;
  width: 100%;
  height: 100%;
  background: ${COLORS.BLACK};
  animation: fadeIn 0.5s;
  top: 0;
  left: 0;
  color: ${COLORS.WHITE};

  /* The animation code */
  @keyframes fadeIn {
    from {
      opacity: 0;
    }
    to {
      opacity: 0.7;
    }
  }

  opacity: 0.7;

  // Initial with hidden
  display: none;
`

class PageFood extends React.Component {
  state = {
    showingModal: false,
    previewVisible: false,
    previewImage: '',
    fileList: [],
    isEdit: false,
  }

  componentDidMount() {
    if (!read('token')) {
      this.props.history.push('/auth')
    }

    const { fetchFoodList = () => {} } = this.props

    fetchFoodList()
  }

  handleInput = (key, event) => {
    this.setState({ [key]: event.target.value })
  }

  handleChangeSelect = value => {
    this.setState({
      type: value,
    })
  }

  handleSubmit = () => {
    const { setFood } = this.props
    const { type, price, name } = this.state

    setFood({
      name,
      price,
      type,
      foodImage: this.state.fileList[0],
    })

    this.setState({
      showingModal: false,
    })

    message.success('เพิ่มรายการอาหารสำเร็จ')
  }

  handleCancel = () => this.setState({ previewVisible: false })

  handlePreview = file => {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true,
    })
  }

  handleChange = ({ fileList }) => {
    let file = fileList[0]
    if (file) {
      file.status = 'done'
      file = [file]
    }
    this.setState({ fileList: file || fileList })
  }

  handleEdit = (url, name, price, type, foodId) => {
    const file = { url: url }

    this.setState({
      isEdit: true,
      showingModal: true,
      previewImage: file,
      previewVisible: true,
      fileList: [{ ...file, uid: Math.random() }],
      name,
      price,
      type,
      url,
      foodId,
    })
  }

  handleSubmitEdit = () => {
    const { editFood } = this.props
    const { type, price, name, url, foodId } = this.state
    
    editFood({
      name,
      price,
      type,
      foodImage: this.state.fileList[0].url ? url : this.state.fileList[0],
      foodId,
    })

    this.setState({
      showingModal: false,
    })

    message.success('แก้ไขรายการอาหารสำเร็จ')
  }

  handleDelete = () => {
    const { deleteFood } = this.props
    const { foodId } = this.state

    deleteFood({ foodId })

    this.setState({ showingModal: false })

    message.success('ลบรายการอาหารสำเร็จ')
  }

  handleAdd = () =>
    this.setState({
      isEdit: false,
      showingModal: true,
      fileList: [],
      name: null,
      price: null,
      type: null,
      previewVisible: false,
    })

  render() {
    const { foodList = [], isLoading = true, error = null } = this.props

    const foodTypeOptions = FOOD_TYPE.map(({ category, id }) => (
      <Option value={category} key={id}>
        {category}
      </Option>
    ))

    if (isLoading) {
      return (
        <Wrapper>
          <Layout>
            <SpinLoading />
          </Layout>
          <NavbarBottom />
        </Wrapper>
      )
    }

    if (!isLoading && error) {
      return (
        <Wrapper>
          <Layout>ไม่สามารถเชื่อมต่อกับฐานข้อมูลได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง</Layout>
          <NavbarBottom />
        </Wrapper>
      )
    }

    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">อัพโหลดรูปภาพ</div>
      </div>
    )

    const enhanceFoodType = [
      {
        id: 0,
        category: 'ทั้งหมด',
      },
      ...FOOD_TYPE,
    ]

    const tabsFood = enhanceFoodType.map(({ category, id }) => {
      const foodItems = foodList
        .filter(({ type }) => type === category || category === 'ทั้งหมด')
        .map(({ url, name, price, foodId, type }) => {
         // const src = url.indexOf('upload') > -1 ? `${DOMAIN_NAME}${url}` : url

          return (
            <CardWrapper key={`food-${foodId}`}>
              <FoodImgWrapper>
                <FoodImg src={url} />
              </FoodImgWrapper>
              <DetailsWrapper>
                <DetailsName> {name} </DetailsName>
                <DetailsPrice> ฿{price} </DetailsPrice>
              </DetailsWrapper>
              <CardEdit
                className="card-edit"
                onClick={() => this.handleEdit(url, name, price, type, foodId)}
              >
                แก้ไข
              </CardEdit>
            </CardWrapper>
          )
        })

      return (
        <TabPane tab={category} key={id}>
          <CardOverride>
            <GridLayout>{foodItems}</GridLayout>
          </CardOverride>
        </TabPane>
      )
    })

    return (
      <Wrapper>
        <Layout>
          <Tabs type="card" title="food" defaultActiveKey="0" key="1">
            {tabsFood}
            <AddFoodButton tab="" key="add-btn" onClick={this.handleAdd}>
              เพิ่มรายการอาหาร
            </AddFoodButton>
          </Tabs>
        </Layout>

        <Modal
          title="เพิ่มรายการอาหาร"
          visible={this.state.showingModal}
          onCancel={() => this.setState({ showingModal: false })}
          footer={[
            <ButtonFooterWrapper isEdit={this.state.isEdit} key={"EDIT_BUTTON"}>
              {this.state.isEdit ? (
                <React.Fragment>
                  <OutlineButton key="delete" onClick={() => this.handleDelete()}>
                    ลบรายการอาหาร
                  </OutlineButton>
                  <Button key="submit" type="primary" onClick={() => this.handleSubmitEdit()}>
                    บันทึกการแก้ไข
                  </Button>
                </React.Fragment>
              ) : (
                <Button
                  disabled={
                    this.state.fileList.length < 1 ||
                    !this.state.price ||
                    !this.state.type ||
                    !this.state.name
                  }
                  key="submit"
                  type="primary"
                  onClick={() => this.handleSubmit()}
                >
                  เพิ่มรายการอาหาร
                </Button>
              )}
            </ButtonFooterWrapper>,
          ]}
          >
          <GridContentWrapper>
            <UploadImgBox
              accept="image/*"
              listType="picture-card"
              fileList={this.state.fileList}
              onPreview={this.handlePreview}
              onChange={this.handleChange}
            >
              {this.state.fileList.length < 1 && uploadButton}
            </UploadImgBox>
            <TextLabel>ชื่ออาหาร</TextLabel>
            <TextInput value={this.state.name} onChange={ev => this.handleInput('name', ev)} />
            <TextLabel>เลือก</TextLabel>
            <Select
              showSearch
              style={{ width: '100%' }}
              placeholder="ประเภท"
              optionFilterProp="children"
              onChange={this.handleChangeSelect}
              value={this.state.type}
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {foodTypeOptions}
            </Select>
            <TextLabel>ราคา (บาท)</TextLabel>
            <TextInput
              value={this.state.price}
              type="number"
              onChange={ev => this.handleInput('price', ev)}
            />
          </GridContentWrapper>
        </Modal>
        <NavbarBottom activeKey="1" />
      </Wrapper>
    )
  }
}

const mapStateToProps = state => ({
  foodList: state.food.foodList,
  isLoading: state.food.isLoading,
  error: state.food.error,
})

const mapDispatchToProps = dispatch => ({
  fetchFoodList: () => dispatch(actions.fetchFoodList()),
  setFood: ({ name, price, foodImage, type, url }) =>
    dispatch(
      actions.setFood({ name, price, foodImage, type, url })
    ),
  editFood: ({ name, price, foodImage, type, foodId, url }) => 
    dispatch(
      actions.editFood({ name, price, foodImage, type, foodId, url })
    ),
  deleteFood: ({ foodId }) => dispatch(actions.deleteFood({ foodId })),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PageFood)
