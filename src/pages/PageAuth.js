import React from 'react'
import styled, { css } from 'styled-components'
import Layout from '../components/Layout'
import Button from '../components/Button'
import { COLORS } from '../styles/colors'
import LandingBG from '../assets/landing-bg.jpg'
import LogoGroup from '../assets/logo_group.svg'
import {
  Input,
  DatePicker,
  Select as AntdSelect,
  Checkbox,
  TimePicker,
  Upload,
  Icon,
  message,
  Progress,
} from 'antd'
import GoogleMap from '../components/GoogleMap'
import axios from 'axios'
import axiosWrapper from '../utils/axios'
import { API_ADDRESS, convertToFormData } from '../utils/api'
import { create } from '../utils/cookie'

const Select = styled(AntdSelect)`
  width: 100%;
`

const LayoutExtend = styled(Layout)`
  background: rgba(0, 0, 0, 0.4);
  align-items: flex-start;
  flex: 1;
  color: ${COLORS.WHITE};
  z-index: 9;
`

const Wrapper = styled.div`
  text-align: center;
`

const OverlayBG = styled.div`
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  position: fixed;
  background: #3f3f3f;
  background-image: url(${LandingBG});
  background-size: cover;
  z-index: 1;
`

const ImgLogo = styled.img`
  width: 220px;
  height: 70px;
`

const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`

const MenuWrapper = styled.div``

const AuthBoxWrapper = styled.div`
  width: 100%;
  flex: 1;
  align-items: center;
  justify-content: center;
  display: flex;
  margin-top: 4em;
`

const AuthBox = styled.div`
  padding: 2em;
  background: rgba(0, 0, 0, 0.7);
  border-radius: 5px;
  width: 600px;
`

const AuthSection = styled.div`
  max-width: 350px;
  margin: 0 auto;
  width: 350px;

  display: grid;
  grid-row-gap: 1.5em;
  grid-template-rows: 1fr;
`

const Form = styled.form`
  max-width: 350px;
  margin: 0 auto;
  width: 350px;

  display: grid;
  grid-row-gap: 1.5em;
  grid-template-rows: 1fr;
`

const AuthHeader = styled.h2`
  font-size: 2em;
  color: ${COLORS.WHITE};
`

const FieldGroup = styled.div`
  text-align: left;
  color: ${COLORS.WHITE};

  .ant-checkbox-wrapper {
    color: ${COLORS.WHITE};
  }
`

const AcceptLinkText = styled.span`
  color: #2299cc;
`

const FieldName = styled.div`
  margin-bottom: 5px;
  text-align: left;
`

const InformationGroup = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 1em;
`

const LoginButton = styled.button`
  padding: 8px $12px;
  background-color: ${props => props.bg || COLORS.RED};
  background-image: ${props =>
  props.bg ||
  `linear-gradient(to right, ${COLORS.GRADIENT_START} 0%, ${COLORS.GRADIENT_STOP} 100%)`};
  color: ${props => props.color || COLORS.WHITE};
  text-align: center;
  border-radius: 5px;
  border: 0 none;
  outline: none;
  cursor: pointer;
  position: relative;
  height: 32px;
  margin-top: 10px;

  &:hover,
  &:active {
    color: ${props => props.color || COLORS.WHITE};
    opacity: 0.9;
  }
`

const AuthSelected = styled.span`
  cursor: pointer;

  ${props =>
  props.selected &&
  css`
      color: ${COLORS.RED};
      text-decoration: underline;
    `}
`

const SelectTime = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 5px;

  white-space: nowrap;

  .ant-time-picker {
    max-width: 100px;
  }
`

const ProgressWrapper = styled.div`
  .ant-progress-text {
    color: ${COLORS.WHITE} !important;
  }
`

class PageAuth extends React.Component {
  state = {
    authState: 'login',
    email: '',
    password: '',
    confirmPassword: '',
    name: '',
    surname: '',
    birthday: '',
    gender: '',
    tel: '',
    openTime: {},
    restaurantAddress: '',
    restaurantDetail: '',
    restaurantName: '',
    restaurantTel: '',
    restaurantType: '',
    latitude: 0,
    longtitude: 0,
    isAccept: false,
    isRestaurant: false,
    isNewRestaurant: false,
    isUploading: false,
    uploadProgress: 0,
    fileList: [],
    file: '',
  }

  setLocation = ({ latitude, longtitude }) => {
    this.setState({ latitude, longtitude })
  }

  handlePreview = file => {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true,
    })
  }

  handleChange = ({ fileList }) => {
    let file = fileList[0]
    if (file) {
      file.status = 'done'
      file = [file]
    }
    this.setState({ fileList: file || fileList })
  }

  handleInput = (key, event, value) => {
    if (value === null || value === undefined) {
      this.setState({ [key]: event.target.value })
    } else {
      this.setState({ [key]: value })
    }
  }

  handleOpenTime = (day, checked, key, time) => {
    const { openTime } = this.state
    if (day && !time) {
      openTime[day] = { checked }
    } else if (key && time) {
      openTime[day][key] = time
    }
    this.setState({ openTime })
  }

  onLogin = async () => {
    const { email, password } = this.state

    const { data = {} } = await axios.post(`${API_ADDRESS}users/login`, {
      email,
      password,
      type: 'owner'
    })

    console.log(data)

    if (data.error) {
      message.error(data.message)
    } else {
      message.success(data.message)
      if (data.activeRestaurant === false) return this.setState({ authState: 'restaurant' })

      create('token', data.token, 86400)
      this.props.history.push('/food')
    }
  }

  onRegis = async () => {
    const { email, password, name, surname, birthday, gender, tel, confirmPassword } = this.state
    if (confirmPassword !== password) return message.error('password not match!')

    const { data = {} } = await axios.post(`${API_ADDRESS}users/register`, {
      email,
      password,
      firstName: name,
      lastName: surname,
      birthday,
      gender,
      tel,
      type: 'owner',
    })

    if (data.error) {
      message.error(data.message)
    } else {
      message.success(data.message)

      const {
        data: { token },
      } = await axios.post(`${API_ADDRESS}users/login`, {
        email,
        password,
        type: 'owner',
      })

      console.log(data)

      create('token', token, 86400)

      this.setState({ authState: 'restaurant' })
    }
  }

  onNewRestaurant = async () => {
    let { openTime } = this.state
    const {
      restaurantName,
      restaurantType,
      restaurantTel,
      restaurantDetail,
      restaurantAddress,
      latitude,
      longtitude,
    } = this.state

    // Filter unchecked day and remove checked values
    openTime = Object.entries(openTime).reduce((result, [key, values]) => {
      // eslint-disable-next-line no-unused-expressions
      values.checked
        ? (result[key] = {
          open: values.open,
          close: values.close,
        })
        : ''
      return result
    }, {})

    const formData = {
      restaurantName,
      restaurantType,
      restaurantTel,
      restaurantDetail,
      restaurantAddress,
      openTime: JSON.stringify(openTime),
      location: JSON.stringify({ latitude, longtitude }),
      file: this.state.file,
    }

    this.setState({ isUploading: true })

    const { data = {} } = await axiosWrapper.post(`${API_ADDRESS}restaurant/new`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      transformRequest: [convertToFormData],
      onUploadProgress: progressEvent => {
        const percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)

        this.setState({ uploadProgress: percentCompleted })
      },
    })

    if (data.error) {
      return message.error(data.message)
    }

    message.success(data.message)
    this.props.history.push('/food')
  }

  beforeUpload = file => {
    this.setState({ file })
  }

  render() {
    const { authState, email, password, name, surname, tel, isAccept, confirmPassword } = this.state

    const authRegis = (
      <React.Fragment>
        <AuthHeader>สมัครสมาชิก</AuthHeader>

        <FieldGroup>
          <FieldName>อีเมล</FieldName>
          <Input
            onChange={event => this.handleInput('email', event)}
            value={email}
            placeholder="อีเมลล์"
            autoComplete="off"
          />
        </FieldGroup>

        <FieldGroup>
          <FieldName>รหัสผ่าน</FieldName>
          <Input
            onChange={event => this.handleInput('password', event)}
            value={password}
            type="password"
            placeholder="รหัสผ่าน"
            autoComplete="off"
          />
        </FieldGroup>
        <FieldGroup>
          <FieldName>ยืนยันรหัสผ่าน</FieldName>
          <Input
            onChange={event => this.handleInput('confirmPassword', event)}
            value={confirmPassword}
            type=" password"
            placeholder=" รหัสผ่าน"
          />
        </FieldGroup>
        <FieldGroup>
          <FieldName>ข้อมูลส่วนตัว</FieldName>
        </FieldGroup>

        <FieldGroup>
          <FieldName>ชื่อ</FieldName>
          <Input
            onChange={event => this.handleInput('name', event)}
            value={name}
            placeholder=" ชื่อ"
          />
        </FieldGroup>

        <FieldGroup>
          <FieldName>นามสกุล</FieldName>
          <Input
            onChange={event => this.handleInput('surname', event)}
            value={surname}
            placeholder=" นามสกุล"
          />
        </FieldGroup>

        <FieldGroup>
          <InformationGroup>
            <Wrapper>
              <FieldName>วันเกิด</FieldName>
              <DatePicker
                onChange={date =>
                  this.handleInput('birthday', null, date.get().format('YYYY-MM-DD'))
                }
                placeholder=" วันเกิด"
              />
            </Wrapper>
            <Wrapper>
              <FieldName>เพศ</FieldName>
              <Select placeholder=" เพศ" onChange={value => this.handleInput('gender', null, value)}>
                <Select.Option value=" unknown">ไม่ระบุ</Select.Option>
                <Select.Option value=" male">ชาย</Select.Option>
                <Select.Option value=" female">หญิง</Select.Option>
              </Select>
            </Wrapper>
          </InformationGroup>
        </FieldGroup>

        <FieldGroup>
          <FieldName>เบอร์โทรศัพท์</FieldName>
          <Input
            onChange={event => this.handleInput('tel', event)}
            value={tel}
            placeholder=" เบอร์โทรศัพท์"
          />
        </FieldGroup>

        <FieldGroup>
          <Checkbox
            onChange={event => this.handleInput('isAccept', null, event.target.checked)}
            checked={isAccept}
          >
            ข้าพเจ้าได้อ่านและยอมรับ <AcceptLinkText>เงื่อนไขการให้บริการ</AcceptLinkText>
          </Checkbox>
        </FieldGroup>

        <Button onClick={() => this.onRegis()}>สมัครสมาชิก</Button>
      </React.Fragment>
    )

    const authLogin = (
      <React.Fragment>
        <Form
          onSubmit={e => {
            e.preventDefault()
            this.onLogin()
          }}
        >
          <AuthHeader>เข้าสู่ระบบ</AuthHeader>

          <FieldGroup>
            <FieldName>อีเมล</FieldName>
            <Input onChange={event => this.handleInput('email', event)} value={email}/>
          </FieldGroup>

          <FieldGroup>
            <FieldName>รหัสผ่าน</FieldName>
            <Input
              onChange={event => this.handleInput('password', event)}
              value={password}
              type="password"
            />
          </FieldGroup>

          <LoginButton type="submit">เข้าสู่ระบบ</LoginButton>
        </Form>
      </React.Fragment>
    )

    const uploadButton = (
      <div>
        <Icon type="plus"/>
        <div className="ant-upload-text">อัพโหลดรูปภาพ</div>
      </div>
    )

    const addRestaurant = (
      <React.Fragment>
        {!this.state.isNewRestaurant ? (
          <Wrapper>
            <Wrapper>ยังไม่มีข้อมูลร้านอาหาร</Wrapper>
            <br/>
            <Button onClick={() => this.setState({ isNewRestaurant: true })}>เพิ่มร้านอาหาร</Button>
          </Wrapper>
        ) : (
          <React.Fragment>
            <AuthHeader>เพิ่มร้านอาหาร</AuthHeader>

            <FieldGroup>
              <FieldName>โลโก้ร้าน</FieldName>
              <Upload
                accept="image/*"
                listType="picture-card"
                beforeUpload={this.beforeUpload}
                fileList={this.state.fileList}
                onPreview={this.handlePreview}
                onChange={this.handleChange}
              >
                {this.state.fileList.length < 1 && uploadButton}
              </Upload>
            </FieldGroup>

            <FieldGroup>
              <FieldName>ชื่อร้าน</FieldName>
              <Input onChange={event => this.handleInput('restaurantName', event)}/>
            </FieldGroup>

            <FieldGroup>
              <FieldName>ประเภท</FieldName>
              <Input onChange={event => this.handleInput('restaurantType', event)}/>
            </FieldGroup>

            <FieldGroup>
              <FieldName>รายละเอียด</FieldName>
              <Input.TextArea
                rows={4}
                onChange={event => this.handleInput('restaurantDetail', event)}
              />
            </FieldGroup>

            <FieldGroup>
              <FieldName>เบอร์โทรศัพท์</FieldName>
              <Input onChange={event => this.handleInput('restaurantTel', event)}/>
            </FieldGroup>

            <FieldGroup>
              <FieldName>วันที่เปิด - ปิด</FieldName>
              <SelectTime>
                <Checkbox onChange={event => this.handleOpenTime('Sunday', event.target.checked)}>
                  วันอาทิตย์
                </Checkbox>
                <Wrapper>
                  <TimePicker
                    disabled={!this.state.openTime.Sunday || !this.state.openTime.Sunday.checked}
                    format={'HH:mm'}
                    placeholder="เปิด"
                    onChange={time =>
                      this.handleOpenTime('Sunday', null, 'open', time.get().format('HH:mm'))
                    }
                  />{' '}
                  -{' '}
                  <TimePicker
                    disabled={!this.state.openTime.Sunday || !this.state.openTime.Sunday.checked}
                    format={'HH:mm'}
                    placeholder="ปิด"
                    onChange={time =>
                      this.handleOpenTime('Sunday', null, 'close', time.get().format('HH:mm'))
                    }
                  />
                </Wrapper>
              </SelectTime>
              <SelectTime>
                <Checkbox onChange={event => this.handleOpenTime('Monday', event.target.checked)}>
                  วันจันทร์
                </Checkbox>
                <Wrapper>
                  <TimePicker
                    disabled={!this.state.openTime.Monday || !this.state.openTime.Monday.checked}
                    format={'HH:mm'}
                    placeholder="เปิด"
                    onChange={time =>
                      this.handleOpenTime('Monday', null, 'open', time.get().format('HH:mm'))
                    }
                  />{' '}
                  -{' '}
                  <TimePicker
                    disabled={!this.state.openTime.Monday || !this.state.openTime.Monday.checked}
                    format={'HH:mm'}
                    placeholder="ปิด"
                    onChange={time =>
                      this.handleOpenTime('Monday', null, 'close', time.get().format('HH:mm'))
                    }
                  />
                </Wrapper>
              </SelectTime>
              <SelectTime>
                <Checkbox onChange={event => this.handleOpenTime('Tuesday', event.target.checked)}>
                  วันอังคาร
                </Checkbox>
                <Wrapper>
                  <TimePicker
                    disabled={!this.state.openTime.Tuesday || !this.state.openTime.Tuesday.checked}
                    format={'HH:mm'}
                    placeholder="เปิด"
                    onChange={time =>
                      this.handleOpenTime('Tuesday', null, 'open', time.get().format('HH:mm'))
                    }
                  />{' '}
                  -{' '}
                  <TimePicker
                    disabled={!this.state.openTime.Tuesday || !this.state.openTime.Tuesday.checked}
                    format={'HH:mm'}
                    placeholder="ปิด"
                    onChange={time =>
                      this.handleOpenTime('Tuesday', null, 'close', time.get().format('HH:mm'))
                    }
                  />
                </Wrapper>
              </SelectTime>
              <SelectTime>
                <Checkbox
                  onChange={event => this.handleOpenTime('Wednesday', event.target.checked)}
                >
                  วันพุธ
                </Checkbox>
                <Wrapper>
                  <TimePicker
                    disabled={
                      !this.state.openTime.Wednesday || !this.state.openTime.Wednesday.checked
                    }
                    format={'HH:mm'}
                    placeholder="เปิด"
                    onChange={time =>
                      this.handleOpenTime('Wednesday', null, 'open', time.get().format('HH:mm'))
                    }
                  />{' '}
                  -{' '}
                  <TimePicker
                    disabled={
                      !this.state.openTime.Wednesday || !this.state.openTime.Wednesday.checked
                    }
                    format={'HH:mm'}
                    placeholder="ปิด"
                    onChange={time =>
                      this.handleOpenTime('Wednesday', null, 'close', time.get().format('HH:mm'))
                    }
                  />
                </Wrapper>
              </SelectTime>
              <SelectTime>
                <Checkbox onChange={event => this.handleOpenTime('Thursday', event.target.checked)}>
                  วันพฤหัส
                </Checkbox>
                <Wrapper>
                  <TimePicker
                    disabled={
                      !this.state.openTime.Thursday || !this.state.openTime.Thursday.checked
                    }
                    format={'HH:mm'}
                    placeholder="เปิด"
                    onChange={time =>
                      this.handleOpenTime('Thursday', null, 'open', time.get().format('HH:mm'))
                    }
                  />{' '}
                  -{' '}
                  <TimePicker
                    disabled={
                      !this.state.openTime.Thursday || !this.state.openTime.Thursday.checked
                    }
                    format={'HH:mm'}
                    placeholder="ปิด"
                    onChange={time =>
                      this.handleOpenTime('Thursday', null, 'close', time.get().format('HH:mm'))
                    }
                  />
                </Wrapper>
              </SelectTime>
              <SelectTime>
                <Checkbox onChange={event => this.handleOpenTime('Friday', event.target.checked)}>
                  วันศุกร์
                </Checkbox>
                <Wrapper>
                  <TimePicker
                    disabled={!this.state.openTime.Friday || !this.state.openTime.Friday.checked}
                    format={'HH:mm'}
                    placeholder="เปิด"
                    onChange={time =>
                      this.handleOpenTime('Friday', null, 'open', time.get().format('HH:mm'))
                    }
                  />{' '}
                  -{' '}
                  <TimePicker
                    disabled={!this.state.openTime.Friday || !this.state.openTime.Friday.checked}
                    format={'HH:mm'}
                    placeholder="ปิด"
                    onChange={time =>
                      this.handleOpenTime('Friday', null, 'close', time.get().format('HH:mm'))
                    }
                  />
                </Wrapper>
              </SelectTime>
              <SelectTime>
                <Checkbox onChange={event => this.handleOpenTime('Saturday', event.target.checked)}>
                  วันเสาร์
                </Checkbox>
                <Wrapper>
                  <TimePicker
                    disabled={
                      !this.state.openTime.Saturday || !this.state.openTime.Saturday.checked
                    }
                    format={'HH:mm'}
                    placeholder="เปิด"
                    onChange={time =>
                      this.handleOpenTime('Saturday', null, 'open', time.get().format('HH:mm'))
                    }
                  />{' '}
                  -{' '}
                  <TimePicker
                    disabled={
                      !this.state.openTime.Saturday || !this.state.openTime.Saturday.checked
                    }
                    format={'HH:mm'}
                    placeholder="ปิด"
                    onChange={time =>
                      this.handleOpenTime('Saturday', null, 'close', time.get().format('HH:mm'))
                    }
                  />
                </Wrapper>
              </SelectTime>
            </FieldGroup>

            <FieldGroup>
              <FieldName>ที่อยู่</FieldName>
              <Input onChange={event => this.handleInput('restaurantAddress', event)}/>
            </FieldGroup>

            <FieldGroup>
              <FieldName>แผนที่</FieldName>
              <GoogleMap setLocation={this.setLocation}/>
            </FieldGroup>

            <Button onClick={() => this.onNewRestaurant()}>เพิ่มร้านอาหาร</Button>

            {this.state.isUploading && (
              <ProgressWrapper>
                Uploading ...
                <Progress percent={this.state.uploadProgress} size="small" default={'default'}/>
              </ProgressWrapper>
            )}
          </React.Fragment>
        )}
      </React.Fragment>
    )

    return (
      <Wrapper>
        <LayoutExtend>
          <HeaderWrapper>
            <Wrapper>
              <ImgLogo src={LogoGroup}/>
            </Wrapper>
            <MenuWrapper>
              <AuthSelected
                selected={authState === 'login'}
                onClick={() => this.setState({ email: '', password: '', authState: 'login' })}
              >
                เข้าสู่ระบบ
              </AuthSelected>{' '}
              |{' '}
              <AuthSelected
                selected={authState === 'regis'}
                onClick={() => this.setState({ email: '', password: '', authState: 'regis' })}
              >
                สมัครสมาชิก
              </AuthSelected>
            </MenuWrapper>
          </HeaderWrapper>
          <AuthBoxWrapper>
            <AuthBox>
              <AuthSection>
                {authState === 'login'
                  ? authLogin
                  : authState === 'regis'
                    ? authRegis
                    : addRestaurant}
              </AuthSection>
            </AuthBox>
          </AuthBoxWrapper>
        </LayoutExtend>

        <OverlayBG/>
      </Wrapper>
    )
  }
}

export default PageAuth
