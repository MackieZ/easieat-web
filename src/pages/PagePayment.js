import React from 'react'
import Layout from '../components/Layout'
import NavbarBottom from '../components/NavbarBottom'
import styled from 'styled-components'
import SpinLoading from '../components/SpinLoading'
import { TableTagWrapper, TableTag } from './PageTable'
import axiosWrapper from '../utils/axios'
import { API_ADDRESS } from '../utils/api'
import { message, Modal } from 'antd'
import COLORS from '../styles/colors'
import Button from '../components/Button'

const Wrapper = styled.div``

const Title = styled.h1`
  display: inline-flex;
  align-items: center;
  font-size: 1.5em;
  font-weight: 600;
  margin-bottom: 0;
`

const Header = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
`

const ListWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 10px;
`

const MenuText = styled.div`
  flex: 2;
`

const AmountText = styled.div`
  flex: 1;
  text-align: right;
`

const PriceText = styled.div`
  flex: 1;
  text-align: right;
`

const RedText = styled.div`
  color: ${COLORS.RED};
`

const Line = styled.div`
  border-bottom: 1px solid ${COLORS.GRAY};
  margin-bottom: 10px;
`

const ButtonFooterWrapper = styled.div`
  width: 100%;
  display: flex;
  margin: 0.5rem 0.5rem 0.5rem auto;
  justify-content: space-between;
  align-items: center;

  > * {
    flex: 1;
  }
`

class PagePayment extends React.Component {
  state = {
    isLoading: true,
    error: null,
    tableList: [],
    showingPayment: false,
    paymentData: {},
    selectedTable: null,
  }

  componentDidMount() {
    const getTableData = async () => {
      try {
        const { data } = await axiosWrapper.get(`${API_ADDRESS}table/list`)

        const tableList = data.map(table => ({
          ...table,
          title: table.tableName,
          code: table.SerialNumber,
          isEditing: false,
          fieldValue: '',
        }))

        this.setState({ isLoading: false, tableList })
      } catch (error) {
        message.error(error)

        // eslint-disable-next-line react/no-did-mount-set-state
        this.setState({ isLoading: false })
      }
    }
    getTableData()
  }

  onSelctedTable = async table => {
    try {
      const { data } = await axiosWrapper.get(`${API_ADDRESS}order/listByowner/${table._id}`)

      const paymentData = data.reduce(
        (result, order) => {
          result.foodList = [...result.foodList, ...order.foodList]
          result.total = result.total + order.price

          return result
        },
        {
          foodList: [],
          total: 0,
        }
      )

      this.setState({ showingPayment: true, paymentData, selectedTable: table })
    } catch (error) {
      message.error(error)
    }
  }

  onPayment = async () => {
    const { selectedTable } = this.state
    const { data } = await axiosWrapper.get(`${API_ADDRESS}order/pay/${selectedTable._id}`)

    if (!data.error) {
      message.success(data.message)
    }

    this.setState({ showingPayment: false })
  }

  render() {
    const {
      isLoading = true,
      error = null,
      tableList = [],
      showingPayment = false,
      paymentData = {},
    } = this.state

    const tables = tableList.map((table, index) => (
      <TableTag
        active={false}
        key={`tableTags-${index}`}
        onClick={() => this.onSelctedTable(table)}
      >
        {table.title}
      </TableTag>
    ))

    const paymentList =
      paymentData.foodList &&
      paymentData.foodList.map((food, index) => (
        <ListWrapper key={index}>
          <MenuText>{food.name}</MenuText>
          <AmountText>{food.quantity}</AmountText>
          <PriceText>{food.price}฿</PriceText>
        </ListWrapper>
      ))

    if (isLoading) {
      return (
        <Wrapper>
          <Layout>
            <SpinLoading />
          </Layout>
          <NavbarBottom activeKey="8" />
        </Wrapper>
      )
    }

    if (!isLoading && error) {
      return (
        <Wrapper>
          <Layout>ไม่สามารถเชื่อมต่อกับฐานข้อมูลได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง</Layout>
          <NavbarBottom activeKey="8" />
        </Wrapper>
      )
    }

    return (
      <Wrapper>
        <Layout>
          <Header>
            <Title>เลือกโต๊ะ</Title>
            <TableTagWrapper>{tables}</TableTagWrapper>
          </Header>
        </Layout>

        <Modal
          title="ชำระเงิน"
          visible={showingPayment}
          onCancel={() => this.setState({ showingPayment: false })}
          footer={[
            <ButtonFooterWrapper key={'BTN_FOOTER'}>
              <React.Fragment>
                <Button key="submit" type="primary" onClick={() => this.onPayment()}>
                  ชำระเงิน
                </Button>
              </React.Fragment>
            </ButtonFooterWrapper>,
          ]}
        >
          <ListWrapper>
            <MenuText>
              <b>รายการ</b>
            </MenuText>
            <AmountText>
              <b>จำนวน</b>
            </AmountText>
            <PriceText>
              <b>ราคา</b>
            </PriceText>
          </ListWrapper>
          {paymentList}
          <Line />
          <ListWrapper>
            <Wrapper>
              <b>รวมทั้งหมด</b>
            </Wrapper>
            <Wrapper>
              <RedText>
                <b>{paymentData.total}</b>
              </RedText>
            </Wrapper>
          </ListWrapper>
        </Modal>

        <NavbarBottom activeKey="8" />
      </Wrapper>
    )
  }
}

export default PagePayment
