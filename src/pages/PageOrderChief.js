import React from 'react'
import Layout from '../components/Layout'
import NavbarBottom from '../components/NavbarBottom'
import styled from 'styled-components'
import { COLORS } from '../styles/colors'
import SpinLoading from '../components/SpinLoading'
import axiosWrapper from '../utils/axios'
import { API_ADDRESS } from '../utils/api'
import { message } from 'antd'

const Wrapper = styled.div``

const Title = styled.h1`
  display: inline-flex;
  align-items: center;
  font-size: 1.5em;
  font-weight: 600;
  margin-bottom: 0;
`

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 1.5em;
`

const RedDot = styled.div`
  display: inline-flex;
  color: ${COLORS.WHITE};
  justify-content: center;
  align-items: center;
  font-size: 16px;
  width: 20px;
  height: 20px;
  background: ${COLORS.RED};
  border-radius: 50%;

  margin: ${props => props.margin};
`

const RecentOrder = styled.div`
  display: inline-flex;
  align-items: center;

  padding: 1em 2em;
  background: ${COLORS.WHITE};
  border-radius: 5px;
  box-shadow: 1px 1px 3px -1px;
  cursor: pointer;

  &:not(:last-child) {
    margin-right: 2em;
  }
`

const RecentOrderWrapper = styled.div`
  margin: 1em 0;
`

const OrderWrapper = styled.div`
  columns: 2;
  column-gap: 2em;
  > div {
    -webkit-column-break-inside: avoid;
    page-break-inside: avoid;
    break-inside: avoid;
  }
`

const OrderBox = styled.div`
  padding: 1em;
  background: #cfcfcf;
  margin-bottom: 1em;
`

const OrderHeader = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

const OrderTableHead = styled.div`
  display: grid;
  grid-template-columns: 60% 20% 20%;
  align-items: center;
  grid-column-gap: 0.5em;
  background: #dfdfdf;

  > div {
    padding: 0.5em 1.5em;
  }
`

const OrderTableContent = styled.div`
  display: grid;
  grid-template-columns: 60% 20% 20%;
  align-items: center;
  grid-column-gap: 0.5em;
  background: #fdfdfd;
  cursor: pointer;

  &:hover {
    background: #ffefef;
  }

  > div {
    padding: 0.5em 1.5em;
  }
`

const Amount = styled.div`
  text-align: center;
`

const FinishButton = styled.div`
  font-size: 12px;
  color: ${COLORS.RED};
  cursor: pointer;
  text-decoration: underline;
  text-align: center;
`

class PageOrderChief extends React.Component {
  state = {
    isLoading: true,
    error: null,
    newOrders: [],
    orderLists: [],
  }

  addOrder = async ({ tableID, tableTitle }, orderIndex) => {
    const res = await axiosWrapper.get(`${API_ADDRESS}order/markinprocess/${tableID}`)

    if (res.data && res.data.error) {
      message.error(res.data && res.data.message)
    }

    const { newOrders = {}, data = [] } = this.state

    const orderLists = data
      .filter(order => order.status === true)
      // .map(order => ({
      //   ...order,
      //   orderList: order.orderList.filter(orderItem => orderItem.status === 2),
      // }))
      .filter(order => order.orderList.length > 0)

    newOrders.splice(orderIndex, 1)
    this.setState({
      newOrders,
      orderLists
    })
  }

  onFinishOrder = async (tableID, foodID, tableIndex, orderIndex) => {
    const { data } = await axiosWrapper.post(`${API_ADDRESS}order/markfinish`, {
      tableID,
      foodID,
    })

    if (data.error) {
      return message.error(data.message)
    }

    const { orderLists = [] } = this.state
    const newOrderLists = [...orderLists]
    newOrderLists[tableIndex].orderList.splice(orderIndex, 1)

    this.setState({
      orderLists: newOrderLists,
    })

    return message.success(data.message)
  }

  componentDidMount() {
    let interval
    const getRestaurantOrder = async () => {
      clearInterval(this.state.interval)
      try {
        const { data } = await axiosWrapper.get(`${API_ADDRESS}order/getorder`)
        console.log(data)
        const newOrders = data
          .filter(order => order.status === false)
          .map(order => ({
            tableID: order.tableID,
            tableTitle: order.tableName,
          }))

        const orderLists = data
          .filter(order => order.status === true)
          .map(order => ({
            ...order,
            orderList: order.orderList.filter(orderItem => orderItem.status === 2),
          }))
          .filter(order => order.orderList.length > 0)

        // eslint-disable-next-line react/no-did-mount-set-state
        this.setState({
          isLoading: false,
          newOrders,
          orderLists,
          data,
        })

        // return data
      } catch (error) {
        message.error(error)

        // eslint-disable-next-line react/no-did-mount-set-state
        this.setState({ isLoading: false })

        // return null
      }

      interval = setTimeout(getRestaurantOrder, 5000)
      this.setState({ interval })
    }
    getRestaurantOrder()
  }

  componentWillUnmount() {
    clearInterval(this.state.interval)
  }

  render() {
    const { isLoading = true, error = null, newOrders, orderLists } = this.state

    const recentOrders = newOrders.map((order, index) => (
      <RecentOrder key={`recent-${index}`} onClick={() => this.addOrder(order, index)}>
        <RedDot margin={'0 1em 0 0'} />
        {order.tableTitle}
      </RecentOrder>
    ))

    const orderListsItem = orderLists.map((order, index) => {
      const orders = order.orderList.map(({ name, quantity, foodId }, orderIndex) => (
        <OrderTableContent
          key={name}
          onClick={() => this.onFinishOrder(order.tableID, foodId, index, orderIndex)}
        >
          <Wrapper>{name}</Wrapper>
          <Amount>{quantity}</Amount>
          <FinishButton>เสร็จสิ้น</FinishButton>
        </OrderTableContent>
      ))

      return (
        <OrderBox key={`order-${index}`}>
          <OrderHeader>{order.tableName}</OrderHeader>
          <OrderTableHead>
            <Wrapper>รายการ</Wrapper>
            <Amount>จำนวน</Amount>
          </OrderTableHead>
          {orders}
        </OrderBox>
      )
    })

    if (isLoading) {
      return (
        <Wrapper>
          <Layout>
            <SpinLoading />
          </Layout>
        </Wrapper>
      )
    }

    if (!isLoading && error) {
      return (
        <Wrapper>
          <Layout>ไม่สามารถเชื่อมต่อกับฐานข้อมูลได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง</Layout>
        </Wrapper>
      )
    }

    return (
      <Wrapper>
        <Layout>
          <Header>
            <Title>
              ออเดอร์ล่าสุด <RedDot margin={'0 10px'}>{newOrders.length}</RedDot>
            </Title>
          </Header>
          <RecentOrderWrapper>{recentOrders}</RecentOrderWrapper>
          <Header>
            <Title>ออเดอร์ที่รับ </Title>
            <Title>
              จำนวนออเดอร์ <RedDot margin={'0 10px'}>{orderLists.length}</RedDot>
            </Title>
          </Header>
          <OrderWrapper>{orderListsItem}</OrderWrapper>
        </Layout>
      </Wrapper>
    )
  }
}

export default PageOrderChief
